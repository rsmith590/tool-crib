/* 
    Author: Robert J. Smith
    Description: Contains angular module and config with 
    routing for tooling crib app

    Version: 1.0
 */

var toolCribApp = angular.module("toolCribApp", ['ngRoute'])
    .config(function($routeProvider){
        $routeProvider
        .when('/', {
            templateUrl: 'partials/toolList.html',
            controller: 'ToolsListController'
        })
        .when('/main-menu', {
            templateUrl: 'partials/main-menu.html',
            controller: 'mainMenuController'
        })
        .when('/checkout/:tool_id', {
            templateUrl: 'partials/checkout.html',
            controller: 'toolsCheckoutController'
        })
        .when('/add-tool', {
            templateUrl: 'partials/add-tool.html',
            controller: 'addToolController'
        })
        .when('/add-job', {
            templateUrl: 'partials/add-job.html',
            controller: 'addJobController'
        })
        .when('/add-operator', {
            templateUrl: 'partials/add-operator.html',
            controller: 'addOperatorController'
        })
        .when('/reports', {
            templateUrl: 'partials/reports.html'
        })
        .when('/description/:tool_id', {
            templateUrl: 'partials/description.html',
            controller: 'descriptionController'
        })
        .when('/low-tools', {
            templateUrl: 'partials/low-tools.html',
            controller: 'lowToolsController'
        })
        .when('/tool-usage-by-job', {
            templateUrl: 'partials/tool-usage-by-job.html',
            controller: 'toolUsageByJobController'
        })
        .when('/tool-usage-by-operator', {
            templateUrl: 'partials/tool-usage-by-operator.html',
            controller: 'toolUsageByOperatorController'
        });
});
