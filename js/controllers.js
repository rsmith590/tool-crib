/* 
    Author: Robert J. Smith
    Description: Contains angular controllers 

    Version: 1.0
 */


toolCribApp.controller("ToolsListController", function(toolsFactory) {
    this.tools = toolsFactory.tools;
});

toolCribApp.controller("mainMenuController", function($scope) {
    
});

toolCribApp.controller("toolsCheckoutController", function($scope, $routeParams, toolsFactory, checkoutsFactory, jobsFactory, operatorsFactory) {
    
   $scope.jobs = jobsFactory.jobs;
   $scope.operators = operatorsFactory.operators;
   $scope.whichTool = $routeParams.tool_id;
   $scope.tool = toolsFactory.tools[$scope.whichTool];
   
   $scope.checkoutTool = function() {
       var qtyRemoved = $scope.checkout.qtyRemoved;
       // Remove Qty from tool object
       var newToolQty = toolsFactory.tools[$scope.whichTool].qty - qtyRemoved;
            if ( newToolQty < 0 ) {
                alert("You do not have enough tools");
                $scope.checkout.qtyRemoved = '';
                return;
            };
       toolsFactory.tools[$scope.whichTool].qty = toolsFactory.tools[$scope.whichTool].qty - qtyRemoved;
       // New Id for Checkouts 
       // Update Reports JSON Object
       var newCheckoutReportId = checkoutsFactory.checkouts.length;
       

            checkoutsFactory.checkouts.push(
               {
                   "id" : newCheckoutReportId,
                   "operatorName" : $scope.checkout.operatorName, 
                   "jobNumber" : $scope.checkout.jobNumber,
                   "qtyRemoved" : $scope.checkout.qtyRemoved,
                   "toolId" : $scope.whichTool
               }
               );
       $scope.checkout.operatorName = '';
       $scope.checkout.jobNumber = '';
       $scope.checkout.qtyRemoved = '';
       alert("Checkout Successfull");
   };
   
});

toolCribApp.controller("descriptionController", function($scope, $routeParams, toolsFactory) {
    var tools = toolsFactory.tools;
    var whichTool = $routeParams.tool_id;
    
    $scope.tool = helperFunctions.findTool(whichTool, tools);
    

});

toolCribApp.controller("addToolController", function($scope, toolsFactory) {
    
    /* getNewId(toolsFactory) takes array of tools counts them returns length ++
     * for new id*/
    var newToolId = helperFunctions.getNewId(toolsFactory.tools);
    
    
    
    $scope.newTool = {};
    
    $scope.newTool.id = newToolId;
    
    
    
    $scope.addTool = function() {
        console.log($scope.newTool);
        toolsFactory.tools.push($scope.newTool);

        alert("Tool Added Successfully");
        $("input").val("");
    };
    
    
});

toolCribApp.controller("lowToolsController", function($scope, toolsFactory) {
    $scope.lowTools = [];
    
    $scope.filterLowerThan = function() {
        for (var i = 0; i < toolsFactory.tools.length; i++) {
            if (toolsFactory.tools[i].qty < $scope.toolLowerThan) {
                $scope.lowTools.push(toolsFactory.tools[i]);
            }   
        }
    };
});

toolCribApp.controller("addJobController", function($scope, jobsFactory) {
    $scope.addJob = function() {
        jobsFactory.jobs.push($scope.newJob);
        alert("Jobs created");
        $('input').val('');
        $('textarea').val('');
    };
});

toolCribApp.controller("addOperatorController", function($scope, operatorsFactory) {
    $scope.addOperator = function() {
        console.log($scope.newOperator);
        operatorsFactory.operators.push($scope.newOperator);
        console.log(operatorsFactory.operators);
        $("input").val("");
        alert("Operator Added Successfully");
    };
});

toolCribApp.controller("toolUsageByOperatorController", function($scope, checkoutsFactory) {
    $scope.toolsByOperator = [];
    
    $scope.filterByOperator = function() {
        for (var i = 0; i < checkoutsFactory.checkouts.length; i++) {
            if (checkoutsFactory.checkouts.name == $scope.operatorName) {
                $scope.toolsByOperator.push(checkoutsFactory.checkouts[i]);
                
            }   
        }
    };
});

toolCribApp.controller("toolUsageByJobController", function($scope) {
    
});
