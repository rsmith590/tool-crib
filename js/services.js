/* 
    Author: Robert J. Smith
    Description: Contains angular services. 

    Version: 1.0
 */


toolCribApp.factory("toolsFactory", function() {
    var data = {};
        data.tools = [
            {
             "id"   : "0",
             "diameter" : ".375",
             "type" : "endmill",
             "qty" : "24",
             "numberOfFlutes" : "4",
             "material" : "carbide",
             "lengthOfCut" : "1.5",
             "coating" : "AlTin",
             "shankDiameter" : ".375",
             "helixAngle" : "30",
             "shankType" : "plain",
             "fluteDirection": "right hand spiral",
             "singleDoubleEnd" : "single",
             "cuttingEdgeProfile" : "standard edge",
             "inchOrMill" : "inch",
             "cuttingDirection" : "right hand",
             "coolantProvision" : "external",
             "endType" : "square",
             "overallLength" : "2"
            },
            {
             "id"   : "1",
             "diameter" : ".250",
             "type" : "drill",
             "qty" : "2",
             "numberOfFlutes" : "2",
             "material" : "carbide",
             "lengthOfCut" : "1.5",
             "coating" : "AlTin",
             "shankDiameter" : ".375",
             "helixAngle" : "30",
             "shankType" : "plain",
             "fluteDirection": "right hand spiral",
             "singleDoubleEnd" : "single",
             "cuttingEdgeProfile" : "standard edge",
             "inchOrMill" : "inch",
             "cuttingDirection" : "right hand",
             "coolantProvision" : "external",
             "endType" : "square",
             "overallLength" : "2"
            },
            {
             "id" : "2",
             "diameter" : ".500",
             "type" : "drill",
             "qty" : "4",
             "numberOfFlutes" : "4",
             "material" : "hss",
             "lengthOfCut" : "1.5",
             "coating" : "AlTin",
             "shankDiameter" : ".375",
             "helixAngle" : "30",
             "shankType" : "plain",
             "fluteDirection": "right hand spiral",
             "singleDoubleEnd" : "single",
             "cuttingEdgeProfile" : "standard edge",
             "inchOrMill" : "inch",
             "cuttingDirection" : "right hand",
             "coolantProvision" : "external",
             "endType" : "point",
             "overallLength" : "2"
            },
            {
             "id"   : "3",
             "diameter" : ".750",
             "type" : "endmill",
             "qty" : "24",
             "numberOfFlutes" : "4",
             "material" : "hss",
             "lengthOfCut" : "1.5",
             "coating" : "AlTin",
             "shankDiameter" : ".375",
             "helixAngle" : "30",
             "shankType" : "plain",
             "fluteDirection": "right hand spiral",
             "singleDoubleEnd" : "single",
             "cuttingEdgeProfile" : "standard edge",
             "inchOrMill" : "inch",
             "cuttingDirection" : "right hand",
             "coolantProvision" : "external",
             "endType" : "square",
             "overallLength" : "2"
            },
            {
             "id"   : "4",
             "diameter" : ".750",
             "type" : "endmill",
             "qty" : "24",
             "numberOfFlutes" : "4",
             "material" : "hss",
             "lengthOfCut" : "1.5",
             "coating" : "AlTin",
             "shankDiameter" : ".375",
             "helixAngle" : "30",
             "shankType" : "plain",
             "fluteDirection": "right hand spiral",
             "singleDoubleEnd" : "single",
             "cuttingEdgeProfile" : "standard edge",
             "inchOrMill" : "inch",
             "cuttingDirection" : "right hand",
             "coolantProvision" : "external",
             "endType" : "square",
             "overallLength" : "2"
            },
            {
             "id"   : "5",
             "diameter" : ".750",
             "type" : "endmill",
             "qty" : "24",
             "numberOfFlutes" : "4",
             "material" : "hss",
             "lengthOfCut" : "1.5",
             "coating" : "AlTin",
             "shankDiameter" : ".375",
             "helixAngle" : "30",
             "shankType" : "plain",
             "fluteDirection": "right hand spiral",
             "singleDoubleEnd" : "single",
             "cuttingEdgeProfile" : "standard edge",
             "inchOrMill" : "inch",
             "cuttingDirection" : "right hand",
             "coolantProvision" : "external",
             "endType" : "square",
             "overallLength" : "2"
            },
            {
             "id"   : "6",
             "diameter" : ".750",
             "type" : "endmill",
             "qty" : "24",
             "numberOfFlutes" : "4",
             "material" : "hss",
             "lengthOfCut" : "1.5",
             "coating" : "AlTin",
             "shankDiameter" : ".375",
             "helixAngle" : "30",
             "shankType" : "plain",
             "fluteDirection": "right hand spiral",
             "singleDoubleEnd" : "single",
             "cuttingEdgeProfile" : "standard edge",
             "inchOrMill" : "inch",
             "cuttingDirection" : "right hand",
             "coolantProvision" : "external",
             "endType" : "square",
             "overallLength" : "2"
            },
            {
             "id"   : "7",
             "diameter" : ".750",
             "type" : "endmill",
             "qty" : "24",
             "numberOfFlutes" : "4",
             "material" : "hss",
             "lengthOfCut" : "1.5",
             "coating" : "AlTin",
             "shankDiameter" : ".375",
             "helixAngle" : "30",
             "shankType" : "plain",
             "fluteDirection": "right hand spiral",
             "singleDoubleEnd" : "single",
             "cuttingEdgeProfile" : "standard edge",
             "inchOrMill" : "inch",
             "cuttingDirection" : "right hand",
             "coolantProvision" : "external",
             "endType" : "square",
             "overallLength" : "2"
            },
            {
             "id"   : "8",
             "diameter" : ".750",
             "type" : "endmill",
             "qty" : "24",
             "numberOfFlutes" : "4",
             "material" : "hss",
             "lengthOfCut" : "1.5",
             "coating" : "AlTin",
             "shankDiameter" : ".375",
             "helixAngle" : "30",
             "shankType" : "plain",
             "fluteDirection": "right hand spiral",
             "singleDoubleEnd" : "single",
             "cuttingEdgeProfile" : "standard edge",
             "inchOrMill" : "inch",
             "cuttingDirection" : "right hand",
             "coolantProvision" : "external",
             "endType" : "square",
             "overallLength" : "2"
            },
            {
             "id"   : "9",
             "diameter" : ".750",
             "type" : "endmill",
             "qty" : "24",
             "numberOfFlutes" : "4",
             "material" : "hss",
             "lengthOfCut" : "1.5",
             "coating" : "AlTin",
             "shankDiameter" : ".375",
             "helixAngle" : "30",
             "shankType" : "plain",
             "fluteDirection": "right hand spiral",
             "singleDoubleEnd" : "single",
             "cuttingEdgeProfile" : "standard edge",
             "inchOrMill" : "inch",
             "cuttingDirection" : "right hand",
             "coolantProvision" : "external",
             "endType" : "square",
             "overallLength" : "2"
            },
            {
             "id"   : "10",
             "diameter" : ".750",
             "type" : "endmill",
             "qty" : "24",
             "numberOfFlutes" : "4",
             "material" : "hss",
             "lengthOfCut" : "1.5",
             "coating" : "AlTin",
             "shankDiameter" : ".375",
             "helixAngle" : "30",
             "shankType" : "plain",
             "fluteDirection": "right hand spiral",
             "singleDoubleEnd" : "single",
             "cuttingEdgeProfile" : "standard edge",
             "inchOrMill" : "inch",
             "cuttingDirection" : "right hand",
             "coolantProvision" : "external",
             "endType" : "square",
             "overallLength" : "2"
            },
            {
             "id"   : "11",
             "diameter" : ".750",
             "type" : "endmill",
             "qty" : "24",
             "numberOfFlutes" : "4",
             "material" : "hss",
             "lengthOfCut" : "1.5",
             "coating" : "AlTin",
             "shankDiameter" : ".375",
             "helixAngle" : "30",
             "shankType" : "plain",
             "fluteDirection": "right hand spiral",
             "singleDoubleEnd" : "single",
             "cuttingEdgeProfile" : "standard edge",
             "inchOrMill" : "inch",
             "cuttingDirection" : "right hand",
             "coolantProvision" : "external",
             "endType" : "square",
             "overallLength" : "2"
            }
        ];
    return data;
});


toolCribApp.factory("checkoutsFactory", function() {
    var data = {};
        data.checkouts = [
            {
                "id" : "0",
                "operatorName" : "Robert Smith",
                "jobNumber" : "1",
                "qtyRemoved" : "3",
                "toolId" : "1"
            },
            {
                "id" : "1",
                "operatorName" : "Robert Smith",
                "jobNumber" : "1",
                "qtyRemoved" : "3",
                "toolId" : "1"
            },
            {
                "id" : "2",
                "operatorName" : "Robert Smith",
                "jobNumber" : "1",
                "qtyRemoved" : "3",
                "toolId" : "1"
            },
        ];
    return data;
});


toolCribApp.factory("jobsFactory", function() {
    var data = {};
    data.jobs = [];
    return data;
});

toolCribApp.factory("operatorsFactory", function() {
    var data = {};
    data.operators = [];
    return data;
});